#ifndef GAME_H_
#define GAME_H_
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <stdlib.h>
#include "Card.h"
#include "Deck.h"
#include "Player.h"


using namespace std;

class Game
{
	public:
		Game();	
		Player Active_Player();
		bool pre;
		bool flop;
		bool turn;
		bool river;
		
		int last_decision;
		int b_rank;
		int n_rank;
		int active_player;
		int non_active_player;
		
		void game_start(Deck d);
		void player_two(Deck d);
		void begin_game();
		
		int Bet();
		int Raise();
		void Call();
		
		void play(Deck d);
		
		void Flop(Deck d);
		void Turn(Deck d);
		void River(Deck d);
		
		void set_active_button();	
		void decision();
		
		void decision_prompt();	
		void bet_decision();
		void post_flop_decision();
		
		int post_flop_bet();
		int your_hand();
		int their_hand();
		void print_pot();
		
		void values_suits(Deck d, Player p);
		void analyze(Deck d);
			
		void flop_card_analyze(Deck d);
		int rank_analysis();
		void your_cards_array();
		void their_cards_array();
		void stack_size();	
		void end_of_game(Deck d);
		int pot;
		int cur_bet;
		int fold;
		int raise;

		Card flop_cards[3];
		int flop_values[3];
		int flop_suits[3];
		
		Card turn_cards[1];
		int turn_values[1];
		int turn_suits[1];
		
		Card river_cards[1];
		int river_values[1];
		int river_suits[1];
		
		const int big_blind;
		const int small_blind;
		
		Player players[2];
		
		int your_values[2];
		int your_suits[2];
		
		int their_values[2];
		int their_suits[2];

		int total_cards[7];
		int total_suits[7];

		int their_total_cards[7];
		int their_total_suits[7];

};
#endif