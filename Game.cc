#include "Game.h"


Game::Game() :  pre(1), flop(1), turn(1), river(1),  last_decision(-1),  b_rank(0), n_rank(0), active_player(1),  non_active_player(2), pot(0), cur_bet(0), fold(0), raise(0), big_blind(100), small_blind(50)
{}

void Game::game_start(Deck d)
{
	
	/* Prints and assigns */
	
	Game g;
	g.players[0].hand(g.players[0], d);

}
void Game::print_pot()
{
	/* Helper function prints the total pot */
	cout << "POT: " << pot << endl;
	
	cout << "------------------" << endl;
}

void Game::begin_game()
{
	Player p;
	p = Active_Player(); //get active player, prompt active player with actions
	cout << "PLAYER " << p.player << " you are the active player" << endl;

}

void Game::stack_size()
{
	/* Prints Player 1, and Player 2 stack size */
	
	cout << "------------------" << endl;
	
	cout << "PLAYER: " << active_player << endl;

	cout << "STACK SIZE: " << players[active_player-1].stack << endl;
	cout << "------------------" << endl;

	cout << "PLAYER: " << non_active_player << endl;

	cout << "STACK SIZE: " << players[non_active_player-1].stack << endl;

	cout << "------------------" << endl;
}

void Game::decision_prompt()
{
	
	/* Based on the last decision of the game prompts the active_player their options, or ends the hand */
	
	if(last_decision == 1)
	{
		bet_decision();		
	}
	if(last_decision == 2 && pre == 1 && raise == 0)
	{	
		post_flop_decision();	
		if(last_decision == 4)
		{
			last_decision = 5;
		}
	}else if(last_decision == 2)
	{
		last_decision = 5;
	}
	if(last_decision == 3)
	{
		players[active_player-1].stack += pot;
		stack_size();
		pre = 0;
		flop = 0;
		turn = 0;
		river = 0;
		fold = 1;

	}
	if(last_decision == 4)
	{
		post_flop_decision();
		if(last_decision == 4)
		{
			last_decision = 5;	
		}
	}



}
	
void Game::bet_decision()
{

	/* Called when active_player is facing a bet, active_player is given the option to RAISE, CALL OR FOLD.
	Sets last decision to the proper intger value. */

	stack_size();
	print_pot();
	int decision;
	cout << "Current Bet: " << cur_bet << endl;
	cout << "RAISE(1), CALL(2), or FOLD(3)" << endl;
	cin >> decision;
	if(cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Please enter an integer ONLY." << endl;
		bet_decision();
	}

	if(decision < 1 || decision > 3)
	{
		cin.clear();
		cin.ignore();
		cout << "ERROR: Did not input RAISE(1), CALL(2) or FOLD(3)" << endl;
		bet_decision();
	}
	if(decision == 1)
	{
		cout << "You decided to: RAISE" << endl;
		last_decision = 1;
		cur_bet = Raise();
	}
	if(decision == 2)
	{
		cout << "You decided to: CALL" << endl;
		last_decision = 2;
		Call();
	}	
	if(decision == 3)
	{
		cout << "You decided to: FOLD" << endl;
		swap(active_player, non_active_player);
		last_decision = 3;
	}
}


void Game::post_flop_decision()
{
	/* Called when active_player is facing a check or is first to act on the flop, turn, or river.
	Set's last decision to the proper integer value. */
	
	stack_size();
	print_pot();
	int decision;
	cout << "CHECK(1), or BET(2)" << endl;
	cin >> decision;
	if(cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Please enter an integer ONLY." << endl;
		bet_decision();
	}
	if(decision < 1 || decision > 2)
	{
		cin.clear();
		cin.ignore();
		cout << "ERROR: Did not input CHECK(1), or BET(2)" << endl;
		post_flop_decision();
	}	
	if(decision == 1)
	{
		cout << "You decided to: CHECK" << endl;
		last_decision = 4;
		swap(active_player, non_active_player);
	}
	if(decision == 2)
	{
		cout << "You decided to: BET" << endl;
		cur_bet = post_flop_bet();
		last_decision = 1;
	}
}

void Game::set_active_button()
{
	/* Used to set the active_player to the left of the button.
	Specifically used to set the first to act player on the flop turn and river. */
	last_decision = -1;
	if(players[0].button == 1)
	{
		active_player = 2;
		non_active_player = 1;
	}else{
		active_player = 1;
		non_active_player = 2;
	}
}

void Game::player_two(Deck d)
{
	/* Used to initalize the second players hand and assign it to the Players array */
	d.size = d.size - 2;
	Game g;
	g.players[1].hand(g.players[1], d);
}



Player Game::Active_Player()
{
	/* Assigns tbe active player and returns the active player */
	Player p;
	for(int i = 0; i < 2; i++)
	{
		if(players[i].active == 1)
		{
			p = players[i];
		}
	}
	return p;
}


int Game::post_flop_bet()
{
	/* Bet prompt and evaluation (pot and stack size) for the post-flop betting round */

	int amount;
	cout << "How much do you want to bet?" << endl;
	cin >> amount;
	if(cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Please enter an integer ONLY." << endl;
		post_flop_bet();
	}	
	if(amount >= players[active_player-1].stack)
	{
		cout << "Player " << active_player << " ALL IN" << endl;
		amount = players[active_player-1].stack;
		players[active_player-1].stack -= amount;

	}else if(amount < big_blind){
		cout << "Bet must be at least equal to the big blind: " << big_blind << endl;
		post_flop_bet();
	}else{
		players[active_player-1].stack -= amount;
		pot += amount;
		cur_bet = amount;
	}
	swap(active_player, non_active_player);
	return amount;
}

void Game::Call()
{
	/* Call prompt and evaluation */

	if(cur_bet >= players[active_player-1].stack)
	{
		cout << "Player " << active_player << " ALL IN" << endl;
		cur_bet = players[active_player-1].stack;
		players[active_player-1].stack -= cur_bet;
		pot += cur_bet;
		players[0].if_call = 0;
		players[1].if_call = 0;
		pre = 0;
		flop = 0;
		turn = 0;
		river = 0;
	
	}else{
	players[active_player-1].stack += players[active_player-1].if_call;
	players[active_player-1].stack -= cur_bet;
	pot -= players[active_player-1].if_call;
	pot += cur_bet;
	players[active_player-1].if_call = 0;
	players[non_active_player-1].if_call = 0;
	}
}


int Game::Raise()
{
	/* Raise prompt and evaluation */
	
	int amount;
	raise = 1;
	cout << "How much do you want to raise to?" << endl;
	cin >> amount;
	if(cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Please enter an integer ONLY." << endl;
		Raise();
	}
	players[active_player-1].stack += players[active_player-1].if_call;
	players[non_active_player-1].if_call = cur_bet;
	if(amount >= players[active_player-1].stack)
	{
		cout << "Player " << active_player << " ALL IN" << endl;
		amount = players[active_player-1].stack;
		players[active_player-1].stack -= amount;
		cur_bet = amount;
		pot-=players[active_player-1].if_call;
		pot += cur_bet;
		print_pot();
	}else if(amount < cur_bet * 2){
		cout << "Raise must be greater than or equal to two times the current bet" << endl;
		Raise();
	}else if(amount >= cur_bet * 2){
		cur_bet = amount;
		players[active_player-1].stack -= amount;
		pot-= players[active_player-1].if_call;
		pot += cur_bet;
		print_pot();
	}
	swap(active_player, non_active_player);
	return amount;
}



/*void Game::swap_active()
{
	swap(players[0].active, players[1].active);
}*/


void Game::play(Deck d)
{
	/* Basis for game play sequence:
		Starts the game, evaluates player decisions and calls corresponding
		decision functions.
		Sends prompts to the command line.
		Evaluates which decision prompts to call based on last_decision boolean value.
	*/

	cout << "HAND START"<< endl;
	cout << "------------------" << endl;

	Player p;
	d.shuffle_deck();
	d.values_suits();
	analyze(d);
	flop_card_analyze(d);


	d.shuffle_deck();
	//Flop(d);

	//Turn(d);

	//River(d);

	your_cards_array();

	//their_cards_array();

	//your_hand();

	//their_hand();

	//isThreeOfaKind();

	//showdown(d);

	pre = 1;
	flop = 1;
	turn = 1;
	river = 1;
	raise = 0;
	set_active_button();	
	
	players[active_player-1].if_call += small_blind;
	players[non_active_player-1].if_call += big_blind;	
	cur_bet = big_blind;
	players[non_active_player-1].stack -= big_blind;
	players[active_player-1].stack -= small_blind;
	pot += big_blind + small_blind;

	cout << "You are PLAYER " << active_player << endl;
	cout << "You are the BUTTON" << endl;
	cout << "You are the Small Blind: " << small_blind << endl;
	system("read -p 'Press Enter to continue.' var");	
	cout << "This is your hand: " << endl;
	game_start(d);
	system("read -p 'Press Enter to continue.' var");
	

	cout << "You are the Small Blind: " << small_blind << endl;


	for(int i = 0; i < 30; i++)

	{
		cout << "-----------------------" << endl;
	}
	
	cout << "You are PLAYER " << non_active_player << endl;
	cout << "You are the Big Blind: " << big_blind << endl;
	system("read -p 'Press Enter to continue.' var");	
	cout << "This is your hand: " << endl;
	player_two(d);


	system("read -p 'Press Enter to continue.' var");
	
	for(int i = 0; i < 30; i++)

	{
		cout << "-----------------------" << endl;
	}
	
	bet_decision();
	while(pre)
	{
		decision_prompt();
		if(last_decision == 5)
		{
			pre = 0;
		}
	}
	if(flop)
	{	
		Flop(d);	
		set_active_button();	
		post_flop_decision();
		if(last_decision == 5)
		{
			flop = 0;
		}
	}
	while(flop)
	{
		decision_prompt();
		if(last_decision == 5)
		{
			flop = 0;
		}
	}
	
	if(turn)
	{
		Turn(d);
		set_active_button();	
		post_flop_decision();
		if(last_decision == 5)
		{
			turn = 0;
		}
	}
	while(turn)
	{
		decision_prompt();
		if(last_decision == 5)
		{
			turn = 0;
		}
	}
	if(river)
	{
		River(d);
		set_active_button();	
		post_flop_decision();
		if(last_decision == 5)
		{
			river = 0;
		}
	}
	while(river)
	{
		decision_prompt();
		if(last_decision == 5)
		{
			river = 0;
		}
	}
	if(pre == 0 && flop == 0 && turn == 0 && river == 0 && fold == 0)
	{
		end_of_game(d);
	}
	last_decision = -1;
	raise = 1;
	fold = 0;
	pot = 0;
	cur_bet = 0;
	swap(players[0].button, players[1].button);

}

void Game::Flop(Deck d)
{
	/* Deals first three community cards: Flop */

	Card c;
	cout << "THE FLOP" << endl;
	for(int i = 0; i < 5; i++)
	{	
	cout << "--------------------------" << endl;
        }	
	d.size = d.size - 4;
	for(int i = 0; i < 3; i++)
	{
		c = d.deal_card();
		flop_cards[i] = c;
		c.print_card(c);
	}
}

void Game::Turn(Deck d)
{
	/* Deals fourth community card: Turn */

	Card c;
	cout << "THE TURN" << endl;
	for(int i = 0; i < 5; i++)
	{	
	cout << "--------------------------" << endl;
	}
	d.size = d.size - 7;

	c = d.deal_card();
	turn_cards[0] = c;
	c.print_card(c);

	int lower = d.size;
	turn_values[0] = d.values[lower+1];
	turn_suits[0] = d.suits[lower+1];
}

void Game::River(Deck d)
{
	/* Deals fifth community card: River */

	Card c;
	cout << "THE RIVER" << endl;
	for(int i = 0; i < 5; i++)
	{
	cout << "--------------------------" << endl;
	}
	d.size = d.size - 8;
	c = d.deal_card();
	river_cards[0] = c;
	c.print_card(c);

	int lower = d.size;
	river_values[0] = d.values[lower+1];
	river_suits[0] = d.suits[lower+1];

}

void Game::analyze(Deck d)
{
	/* Creates arrays of values and suits for both players hand */

	int _size = 52;
	int lower = _size - 2;

	
	for(int i = lower; i < _size; i++)
	{
		your_values[i-lower] = d.values[i];
		your_suits[i-lower] = d.suits[i];
	}
	int lower_2 = lower - 2;
	for(int j = lower_2; j < lower; j++)
	{
		their_values[j-(lower_2)] = d.values[j];
		their_suits[j-(lower_2)] = d.suits[j];
	}
}

void Game::flop_card_analyze(Deck d)
{
	/* Creating value and suit arrays for three community flop cards. */

	int _size = 52;

	int lower = _size - 7;

	for(int i = lower; i < lower + 3 ; i++)
	{
		flop_values[i-lower] = d.values[i];
		flop_suits[i-lower] = d.suits[i];

	}
}

void Game::your_cards_array()
{
	/* Creating array of player cards, flop cards, turn cards and river cards. */

	for(int i = 0; i < 1; i++)
	{
		total_cards[i+6] = river_values[0];
		for(int i = 0; i < 1; i++)
		{
			total_cards[i+5] = turn_values[0];
			for(int i = 0; i < 2; i++)
			{
				total_cards[i+3] = your_values[i];
				for(int j = 0; j < 3; j++)
				{
					total_cards[j] = flop_values[j];
				} 
			}
		}
	}

	int n = sizeof(total_cards)/sizeof(total_cards[0]);
	sort(total_cards, total_cards+n);

	for(int i = 0; i < 1; i++)
	{
		total_suits[i+6] = river_suits[0];
		for(int i = 0; i < 1; i++)
		{
			total_suits[i+5] = turn_suits[0];
			for(int i = 0; i < 2; i++)
			{
				total_suits[i+3] = your_suits[i];
				for(int j = 0; j < 3; j++)
				{
					total_suits[j] = flop_suits[j];
				} 
			}
		}
	}
	int x = sizeof(total_suits)/sizeof(total_suits[0]);
	sort(total_suits, total_suits+x);
}

void Game::their_cards_array()
{
	/* Creating array of othere players cards, flop cards, turn cards and river cards. */

	for(int i = 0; i < 1; i++)
	{
		their_total_cards[i+6] = river_values[0];
		for(int i = 0; i < 1; i++)
		{
			their_total_cards[i+5] = turn_values[0];
			for(int i = 0; i < 2; i++)
			{
				their_total_cards[i+3] = their_values[i];
				for(int j = 0; j < 3; j++)
				{
					their_total_cards[j] = flop_values[j];
				} 
			}
		}
	}

	int n = sizeof(their_total_cards)/sizeof(their_total_cards[0]);
	sort(their_total_cards, their_total_cards+n);

	for(int i = 0; i < 1; i++)
	{
		their_total_suits[i+6] = river_suits[0];
		for(int i = 0; i < 1; i++)
		{
			their_total_suits[i+5] = turn_suits[0];
			for(int i = 0; i < 2; i++)
			{
				their_total_suits[i+3] = their_suits[i];
				for(int j = 0; j < 3; j++)
				{
					their_total_suits[j] = flop_suits[j];
				} 
			}
		}
	}
	int x = sizeof(their_total_suits)/sizeof(their_total_suits[0]);
	sort(their_total_suits, their_total_suits+x);


}


int Game::your_hand()
{
	/* Evaluating player one cards using value and suit arrays for player cards,
	flop cards, turn cards and river cards. Using these arrays to assign ranks 
	based on the type of hand (i.e. Straight Flush) */

	int kind = 0;


	bool straight = false;
	bool flush = false;
	bool four = false;
	bool three = false;
	bool two = false;
	bool one = false;
	int rank = 0;

	int match_vals = 0;
	int match_suits = 0;	

	for(int i = 0; i < 6; i++)
	{
		if(total_cards[i]+1 != total_cards[i+1])
		{
			match_vals = 0;
		}
		if(total_cards[i]+1 == total_cards[i+1])
		{
			match_vals += 1;
			if(match_vals == 4)
			{
				straight = true;
			}
		}
	}

	for(int i = 0; i < 6; i++)
	{	
		if(total_suits[i] != total_suits[i+1])
		{
			match_suits = 0;
		}
		if(total_suits[i] == total_suits[i+1])
		{
			match_suits += 1;
			if(match_suits == 4)
			{
				flush = true;
			}
		}
	}
	
	for(int i = 0; i < 6; i++)
	{
		if(total_cards[i] != total_cards[i+1])
		{
			if(kind == 1)
			{
				if(one == true)
				{
					two = true;
				}
				one = true;
			}else if(kind == 2){
				three = true;
			}else if(kind == 3){
				four = true;
			}
			kind = 0;

		}
		if(total_cards[i] == total_cards[i+1])
		{
			kind++;
		}	
	}
	if(straight && flush)
	{
		rank = 1;
	}
	else if(four == true){
		rank = 2;
	}else if(one == true && three == true){
		rank = 3;
	}else if(flush){
		rank = 4;
	}else if(straight){
		rank = 5;
	}else if(three){
		rank = 6;
	}else if(two){
		rank = 7;
	}else if(one){	
		rank = 8;
	}else{
		rank = 9;
	}
	return rank;
}


int Game::their_hand()
{

	/* Evaluating player two cards using value and suit arrays for player cards,
	flop cards, turn cards and river cards. Using these arrays to assign ranks 
	based on the type of hand (i.e. Straight Flush) */

	int kind = 0;




	bool straight = false;
	bool flush = false;
	bool four = false;
	bool three = false;
	bool two = false;
	bool one = false;
	int rank = 0;

	int match_vals = 0;
	int match_suits = 0;	
	
	for(int i = 0; i < 6; i++)
	{
		if(their_total_cards[i]+1 != their_total_cards[i+1])
		{
			match_vals = 0;
		}
		if(their_total_cards[i]+1 == their_total_cards[i+1])
		{
			match_vals += 1;
			if(match_vals == 4)
			{
				straight = true;
			}
		}
	}

	for(int i = 0; i < 6; i++)
	{	
		if(their_total_suits[i] != their_total_suits[i+1])
		{
			match_suits = 0;
		}
		if(their_total_suits[i] == their_total_suits[i+1])
		{
			match_suits += 1;
			if(match_suits == 4)
			{
				flush = true;
			}
		}
	}


	for(int i = 0; i < 6; i++)
	{
		if(their_total_cards[i] != their_total_cards[i+1])
		{
			if(kind == 1)
			{
				if(one == true)
				{
					two = true;
				}
				one = true;
			}else if(kind == 2){
				three = true;
			}else if(kind == 3){
				four = true;
			}
			kind = 0;

		}
		if(their_total_cards[i] == their_total_cards[i+1])
		{
			kind++;
		}	
	}

	if(straight && flush)
	{
		rank = 1;
	}
	else if(four == true){
		rank = 2;
	}else if(one == true && three == true){
		rank = 3;
	}else if(flush){
		rank = 4;
	}else if(straight){
		rank = 5;
	}else if(three){
		rank = 6;
	}else if(two){
		rank = 7;
	}else if(one){	
		rank = 8;
	}else{
		rank = 9;
	}
	return rank;
}




void Game::end_of_game(Deck d)
{
	/* Using rankings to compare player hands and evaluate a winner.
	Player with higher hand ranking wins.
	If both players have same ranking corresponding function is called.
	*/
	cout << "SHOWDOWN" << endl;
	cout << "---------------------" << endl;
	set_active_button();
	cout << "PLAYER " << active_player << " hand: " << endl;
	game_start(d);
	cout << "PLAYER " << non_active_player << " hand: " << endl;
	//their_cards_array();
	//your_cards_array();
	player_two(d);
	their_cards_array();
	your_cards_array();
	b_rank = your_hand();
	n_rank = their_hand();
	Flop(d);
	Turn(d);
	River(d);
	//their_cards_array();
	//your_cards_array();
	if(b_rank < n_rank)
	{
		cout << "PLAYER " << active_player << " WINS: " << pot << endl;
		players[non_active_player-1].stack += pot;
	}
	if(b_rank > n_rank)
	{
		cout << "PLAYER " << non_active_player << " WINS: " << pot << endl;
		players[non_active_player-1].stack += pot;
	}
	if(b_rank == n_rank)
	{
		int z = rank_analysis();
		if(z == 3)
		{
			cout << "CHOP" << endl;
			players[0].stack += pot*.5;
			players[1].stack += pot*.5;
			return;
		}
		if(z == -1)
		{
			cout << "ERROR" << endl;
		}
		cout << "PLAYER " << z << " WINS: " << pot << endl;
		players[z-1].stack += pot;
	}
}

int Game::rank_analysis()
{
	/*Both players have same ranking, winner must be determined based on who 
	has the best overall hand (i.e. who has the highest card). 
	*/
	int nap_val = -1;
	int ap_val = -1;
	if(b_rank == 9 && n_rank == 9)
	{
		for(int i = 0; i < 5; i++)
		{
			if(total_cards[6-i] == their_total_cards[6-i])
			{
				continue;
			}else if(total_cards[6-i] > their_total_cards[6-1])
				{
					return active_player;
				}else if(total_cards[6-i] < their_total_cards[6-1]){
					return non_active_player;
				}
				
			

		}
		return 3;	
	}
	
	if(b_rank == 8 && n_rank == 8)
	{
		for(int i = 0; i < 6; i++)
		{
			if(total_cards[i] != total_cards[i+1])
			{
				continue;
			}else if(total_cards[i] == total_cards[i+1])
				{
					ap_val = total_cards[i]; 	
				}

		}	
		
		for(int i = 0; i < 6; i++)
		{
			if(their_total_cards[i] != their_total_cards[i+1])
			{
				continue;
			}else if(their_total_cards[i] == their_total_cards[i+1])
				{
					nap_val = their_total_cards[i]; 	
				
				}

		}
		if(nap_val > ap_val)
		{
			return non_active_player;
		}
		if(nap_val < ap_val)
		{
			return active_player;
		}
		if(nap_val == ap_val)
		{
			for(int i = 0; i < 5; i++)
			{
				if(total_cards[6-i] == their_total_cards[6-i])
				{
					continue;
				}else if(total_cards[6-i] > their_total_cards[6-i])
				{
					return active_player;
				}else if(total_cards[6-i] < their_total_cards[6-i]){
					return non_active_player;
				}
			}	

		}		
	}
	return 3;

	if(b_rank == 7 && n_rank == 7)
	{
		int nap_val_1 = -1;
		int ap_val_1 = -1;
		for(int i = 0; i < 6; i++)
		{
			if(total_cards[i] != total_cards[i+1])
			{
				continue;
			}else if(total_cards[i] == total_cards[i+1])
				{
					if(ap_val > 0)
					{
						ap_val_1 = total_cards[i];

					}else{
						ap_val = total_cards[i]; 	
					}
				}

		}
		
		for(int i = 0; i < 6; i++)
		{
			if(their_total_cards[i] != their_total_cards[i+1])
			{
				continue;
			}else if(their_total_cards[i] == their_total_cards[i+1])
				{
					if(nap_val > 0)
					{
						nap_val_1 = their_total_cards[i];
					}else{
						nap_val = their_total_cards[i]; 	
					}
				}

		}
		if(nap_val_1 > ap_val_1)
		{
			return non_active_player;
		}
		if(nap_val_1 < ap_val_1)
		{
			return active_player;
		}
		if(nap_val_1 == ap_val_1)
		{
			if(nap_val > ap_val)
			{
				return non_active_player;
			}
			if(nap_val < ap_val)
			{
				return active_player;
			}
			return 3;
		}
	}
	
	if(b_rank == 6 && n_rank == 6)
	{
		for(int i = 0; i < 6; i++)
		{
			if(total_cards[i] != total_cards[i+1])
			{
				continue;
			}else if(total_cards[i] == total_cards[i+1])
				{
					ap_val = total_cards[i]; 	
				}

		}	
		
		for(int i = 0; i < 6; i++)
		{
			if(their_total_cards[i] != their_total_cards[i+1])
			{
				continue;
			}else if(their_total_cards[i] == their_total_cards[i+1])
				{
					nap_val = their_total_cards[i]; 	
				
				}

		}
		if(nap_val > ap_val)
		{
			return non_active_player;
		}
		if(nap_val < ap_val)
		{
			return active_player;
		}
		if(nap_val == ap_val)
		{
			for(int i = 0; i < 5; i++)
			{
				if(total_cards[6-i] == their_total_cards[6-i])
				{
					continue;
				}else if(total_cards[6-i] > their_total_cards[6-i])
				{
					return active_player;
				}else if(total_cards[6-i] < their_total_cards[6-i]){
					return non_active_player;
				}
			}	
			return 3;
		}

	if((b_rank == 5 && n_rank == 5)||(b_rank == 1 && n_rank == 1))
	{
		for(int i = 0; i < 6; i++)
		{
			int ap_match_vals = 0;
			int nap_match_vals = 0;
			if(total_cards[i]+1 != total_cards[i+1])
			{
				if(ap_match_vals >= 4)
				{
					ap_val = total_cards[i];		
				}
				ap_match_vals = 0;
			}
			if(total_cards[i]+1 == total_cards[i+1])
			{
				ap_match_vals += 1;
			
			}
			if(their_total_cards[i]+1 != their_total_cards[i+1])
			{
				if(nap_match_vals >= 4)
				{
					nap_val = total_cards[i];		
				}
				nap_match_vals = 0;
			}	
			if(their_total_cards[i]+1 == their_total_cards[i+1])
			{
				nap_match_vals += 1;
			}
		}
	}
		if(ap_val > nap_val)
		{
			return active_player;
		}
		if(ap_val < nap_val)
		{
			return non_active_player;
		}
		if(ap_val == nap_val)
		{
			return 3;
		}
	if(b_rank == 4 && n_rank == 4)
	{
		int ap_match_suits = 0;
		int nap_match_suits = 0;
		for(int i = 0; i < 6; i++)
		{	
			if(total_suits[i] != total_suits[i+1])
			{	
				if(ap_match_suits >= 4)
				{
					ap_val = total_suits[i]; 
				}
				ap_match_suits = 0;
			}
			if(total_suits[i] == total_suits[i+1])
			{
				ap_match_suits += 1;
			}	
			if(their_total_suits[i] != their_total_suits[i+1])
			{
				if(nap_match_suits >= 4)
				{
					ap_val = total_suits[i];
				}
				nap_match_suits = 0;
			}
			if(their_total_suits[i] == their_total_suits[i+1])
			{
				ap_match_suits += 1;
			}
		}
		if(ap_val > nap_val)
		{
			return active_player;
		}
		if(ap_val < nap_val)
		{
			return non_active_player;
		}
		if(ap_val == nap_val)
		{
			return 3;
		}
	}
	if(b_rank == 3 && n_rank == 3)
	{
		int ap_match_val = 0;
		int nap_match_val = 0;
		int ap_val_1 = 0;
		int nap_val_1 = 0;

		for(int i = 0; i < 6; i++)
		{
			if(total_cards[i] != total_cards[i+1])
			{
				if(ap_match_val == 1 && (total_cards[i] > ap_val_1))
				{
						ap_val_1 = total_cards[i];
				}
				if(ap_match_val == 2 && (total_cards[i] > ap_val))
				{
					ap_val = total_cards[i];
				}
				ap_match_val = 0;
			}
			if(total_cards[i] == total_cards[i+1])
			{
				ap_match_val += 1;
			}
			if(their_total_cards[i] != their_total_cards[i+1])
			{
				if(nap_match_val == 1 && (their_total_cards[i] > nap_val_1))
				{
						nap_val_1 = their_total_cards[i];
				}
				if(nap_match_val == 2 && (their_total_cards[i] > nap_val))
				{
					nap_val = their_total_cards[i];
				}
				nap_match_val = 0;
			}
			if(their_total_cards[i] == their_total_cards[i+1])
			{
				nap_match_val += 1;
			}	
		}	
		if(ap_val > nap_val)
		{
			return active_player;
		}
		if(ap_val < nap_val)
		{
			return non_active_player;
		}
		if(ap_val == nap_val)
		{
			if(ap_val_1 > nap_val_1)
			{
				return active_player;
			}
			if(ap_val_1 < nap_val_1)
			{
				return non_active_player;
			}
			if(ap_val_1 == nap_val_1)
			{
				return 3;
			}
		}
	}
	return -1;
	
}
}
