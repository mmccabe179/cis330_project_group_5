#ifndef PLAYER_H_
#define PLAYER_H_
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <stdlib.h>
#include "Card.h"
#include "Deck.h"

using namespace std;

class Player
{
	public:
		Player();
		Card cards[2];
		void hand(Player p, Deck d);
		bool button;
		bool active;
		int player;
		int stack;
		int if_call;

	//protected:
		//Card cards[1];


};

#endif