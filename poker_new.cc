#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <stdlib.h>

using namespace std;

class Card
{
	public:
		Card();
		int create_card();
		void init_card(int);
		void print_card(Card);
		int value;
		int suit;
		int card;
		string values;
		string suits;
};

class Deck
{
	public:
		Deck();
		int size;
		void original_deck();
		void print_deck();
		void shuffle_deck();
		Card deal_card();
		void values_suits();
		int values[52];
		int suits[52];
		Card deck[52];
};

class Player
{
	public:
		Player();
		Card cards[2];
		void hand(Player p, Deck d);
		bool button;
		bool active;
		int player;
		int stack;
		int if_call;

	//protected:
		//Card cards[1];


};


class Game
{
	public:
		Game();	
		Player Active_Player();
		bool pre;
		bool flop;
		bool turn;
		bool river;
		
		int last_decision;
		int b_rank;
		int n_rank;
		int active_player;
		int non_active_player;
		
		void game_start(Deck d);
		void AI_game(Deck d);
		void begin_game();
		
		int Bet();
		int Raise();
		void Call();
		
		void play(Deck d);
		
		void Flop(Deck d);
		void Turn(Deck d);
		void River(Deck d);
		
		void set_active_button();	
		void decision();
		
		void decision_prompt();	
		void bet_decision();
		void post_flop_decision();
		
		int post_flop_bet();
		int your_hand();
		int their_hand();
		void print_pot();
		
		void values_suits(Deck d, Player p);
		void analyze(Deck d);
			
		void flop_card_analyze(Deck d);
		int rank_analysis();
		void your_cards_array();
		void their_cards_array();
		void stack_size();	
		void end_of_game(Deck d);
		int pot;
		int cur_bet;
		int fold;
		int raise;

		Card flop_cards[3];
		int flop_values[3];
		int flop_suits[3];
		
		Card turn_cards[1];
		int turn_values[1];
		int turn_suits[1];
		
		Card river_cards[1];
		int river_values[1];
		int river_suits[1];
		
		const int big_blind;
		const int small_blind;
		
		Player players[2];
		
		int your_values[2];
		int your_suits[2];
		
		int their_values[2];
		int their_suits[2];

		int total_cards[7];
		int total_suits[7];

		int their_total_cards[7];
		int their_total_suits[7];

};

Game::Game() : big_blind(100), small_blind(50), active_player(1), non_active_player(2), pot(0), cur_bet(0), last_decision(-1), pre(1), flop(1), turn(1), river(1), b_rank(0), n_rank(0), fold(0), raise(0)
{}

Card::Card() : value(0), suit(0), card(0)
{}

int Card::create_card()
{
	return card;
}

void Card::init_card(int new_card)
{

	//Intializes a card given an integer referring to the index of 
	// Card deck[52].

	if(new_card >= 0 && new_card < 52)
	{
		card = new_card;
		value = card%13;
		//cout << value << endl;
		suit = card/13;
	}
	else
	{
		card = 0;
		value = 0;
		suit = 0;
	}
	
}

void Card::print_card(Card n)
{
	//Helper function to print a card.

	string Suits[] = {"Hearts", "Diamonds", "Spades", "Clubs"};
	string Values[] = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
	//cout << n.value << endl;
	cout << Values[n.value] << " of " << Suits[n.suit] << endl;
	
}


int main()
{
	
	Card c;
	Card();
	c.create_card();
	Deck d;
	Deck();
	//d.shuffle_deck();
	d.values_suits();
	Player p1;
	Player p2;
	Player();
	Game g;
	Game();
	g.players[0] = p1;
	g.players[1] = p2;
	g.players[0].player = 1;
	g.players[1].player = 2;
	g.players[0].active = 1;
	g.players[0].button = 1;
	Game();
	while(g.players[1].stack > 0 && g.players[0].stack > 0)
	{
		g.play(d);
	}
}

Deck::Deck() : size(51) {}


void Deck::original_deck()
{
	// Intializes the deck Ace through King, to each suit.
	// Total of 13 cards * 4 suits = 52.
	Deck d;
	Card new_card;
	for(int i = 0; i < 52; i++)
	{
		deck[i].init_card(i);
		//deck[i].print_card(deck[i]);
	}
}

void Deck::print_deck()
{
	//Prints Card deck[52] within the deck class.

	for(int i = 0; i < 52; i++)
	{
		
		deck[i].print_card(deck[i]);	
	}
}


void Deck::shuffle_deck()
{
	//Randomizes Card deck[52] wtihin the deck clss.
	srand(time(0));
	original_deck();
	for(int i = 0; i < 52; i++)
	{
		int j = rand() % 52;
		swap(deck[i], deck[j]);
		
		//print_deck();
	}
}
void Deck::values_suits()
{
	//Intializes the values and suits to each card in Card deck[52].
	
	int _size = 52;

	for(int i = 0; i < _size; i++)
	{
		suits[i] = deck[i].suit;
		values[i] = deck[i].value;
<<<<<<< HEAD
		//cout << suits[i] << endl;
		//cout << deck[i].value << endl;
=======
>>>>>>> 76e3aa6669a6076cd285e0071c671e14634d7e5f
	}
}


Card Deck::deal_card()
{
	//Returns a card from the "top" of the Card deck[52].
	
	Card c;
	int new_size = size;
	c = deck[new_size];
	size--;
	return c;

}

Player::Player() : stack(10000), button(0), player(0), active(0), if_call(0) 
{}


void Player::hand(Player p, Deck d)
{
	//Prints and assigns the inputted player's hand.
	
	for(int i = 0;i < 2; i++)
	{
		p.cards[i] = d.deal_card();
		p.cards[i].print_card(p.cards[i]);

	}
}


void Game::game_start(Deck d)
{
	
	//Prints and assigns 
	
	Game g;
	g.players[0].hand(g.players[0], d);

}
void Game::print_pot()
{
	//Helper function prints the total pot.
	cout << "POT: " << pot << endl;
	
	cout << "------------------" << endl;
}

void Game::begin_game()
{
	Player p;
	p = Active_Player(); //get active player, prompt active player with actions
	cout << "PLAYER " << p.player << " you are the active player" << endl;

}

void Game::stack_size()
{
	//Prints Player 1, and Player 2 stack size.
	
	cout << "------------------" << endl;
	
	cout << "PLAYER: " << active_player << endl;

	cout << "STACK SIZE: " << players[active_player-1].stack << endl;
	cout << "------------------" << endl;

	cout << "PLAYER: " << non_active_player << endl;

	cout << "STACK SIZE: " << players[non_active_player-1].stack << endl;

	cout << "------------------" << endl;
}

void Game::decision_prompt()
{
	
	//Based on the last decision of the game prompts the active_player their options, or ends the hand.
	
	if(last_decision == 1)
	{
		bet_decision();		
	}
	if(last_decision == 2 && pre == 1 && raise == 0)
	{	
		post_flop_decision();	
		if(last_decision == 4)
		{
			last_decision = 5;
		}
	}else if(last_decision == 2)
	{
		last_decision = 5;
	}
	if(last_decision == 3)
	{
		players[active_player-1].stack += pot;
		stack_size();
		pre = 0;
		flop = 0;
		turn = 0;
		river = 0;
		fold = 1;

	}
	if(last_decision == 4)
	{
		post_flop_decision();
		if(last_decision == 4)
		{
			last_decision = 5;	
		}
	}



}
	
void Game::bet_decision()
{

	//Called when active_player is facing a bet, active_player is given the option to RAISE, CALL OR FOLD.
	//Sets last decision to the proper intger value.

	stack_size();
	print_pot();
	int decision;
	cout << "Current Bet: " << cur_bet << endl;
	cout << "RAISE(1), CALL(2), or FOLD(3)" << endl;
	cin >> decision;
	if(cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Please enter an integer ONLY." << endl;
		bet_decision();
	}

	if(decision < 1 || decision > 3)
	{
		cin.clear();
		cin.ignore();
		cout << "ERROR: Did not input RAISE(1), CALL(2) or FOLD(3)" << endl;
		bet_decision();
	}
	if(decision == 1)
	{
		cout << "You decided to: RAISE" << endl;
		last_decision = 1;
		cur_bet = Raise();
	}
	if(decision == 2)
	{
		cout << "You decided to: CALL" << endl;
		last_decision = 2;
		Call();
	}	
	if(decision == 3)
	{
		cout << "You decided to: FOLD" << endl;
		swap(active_player, non_active_player);
		last_decision = 3;
	}
}


void Game::post_flop_decision()
{
	//Called when active_player is facing a check or is first to act on the flop, turn, or river.
	//Set's last decision to the proper integer value.
	
	stack_size();
	print_pot();
	int decision;
	cout << "CHECK(1), or BET(2)" << endl;
	cin >> decision;
	if(cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Please enter an integer ONLY." << endl;
		bet_decision();
	}
	if(decision < 1 || decision > 2)
	{
		cin.clear();
		cin.ignore();
		cout << "ERROR: Did not input CHECK(1), or BET(2)" << endl;
		post_flop_decision();
	}	
	if(decision == 1)
	{
		cout << "You decided to: CHECK" << endl;
		last_decision = 4;
		swap(active_player, non_active_player);
	}
	if(decision == 2)
	{
		cout << "You decided to: BET" << endl;
		cur_bet = post_flop_bet();
		last_decision = 1;
	}
}
/*
void Game::post_flop_decision_two()
{
	stack_size();
	int decision;
	cin >> decision;
	if(decision => 1)
	{
		cout << 
	}
	if(decision == 2)
	{
		cout << "You decided to: BET" << endl;
		cur_bet = post_flop_bet();
	}
	if(decision == 3)
	{
		cout << "You decided to: CALL" << endl;
		Call();
	}
	if(decision == 4)
	{
		cout << "You decided to: CHECK" << endl;
	}
	if(decision > 5)
	{
		cout << "Not a valid decision" << endl;
	}
	return decision;
}
*/
void Game::set_active_button()
{
	//Used to set the active_player to the left of the button.
	//Specifically used to set the first to act player on the flop turn and river.
	last_decision = -1;
	if(players[0].button == 1)
	{
		active_player = 2;
		non_active_player = 1;
	}else{
		active_player = 1;
		non_active_player = 2;
	}
}
/*
void Game::decision()
{
	stack_size();
	int decision;
	cin >> decision;
 	cout << "You are the Big Blind: " << big_blind << endl;
	cout << "Do you BET(1), CALL(2), RAISE(3) or FOLD(4)? " << endl;
	if(decision == 1)
	{
		cout << "You decided to: BET" << endl;
		cur_bet = big_blind;
		cout << "must bet at least " << 2*cur_bet << endl;
		cur_bet = post_flop_bet();
	}
	if(decision == 2)
	{
		cout << "You decided to: CALL" << endl;
		Call();
	}	
	if(decision == 3)
	{
		cout << "You decided to: RAISE" << endl;
		Raise();
	}
	if(decision == 4)
	{
		cout << "You decided to: FOLD" << endl;
		Fold();
	}
	if(decision > 4)
	{
		cout << "Not a valid decision" << endl;
	}
	
}
*/
void Game::AI_game(Deck d)
{
	d.size = d.size - 2;
	Game g;
	g.players[1].hand(g.players[1], d);
}

/*
int Game::Bet()
{
	int amount;

	cout << "How much do you want to bet?" << endl;
	cin >> amount;
	cout << "BET: $" << amount << endl;
	
	return amount;
}*/

Player Game::Active_Player()
{
	Player p;
	for(int i = 0; i < 2; i++)
	{
		if(players[i].active == 1)
		{
			p = players[i];
		}
	}
	return p;
}

/*
int Game::Bet()
{
	int amount;
	cout << "How much do you want to bet?" << endl;
	cin >> amount;
	if(amount >= players[active_player-1].stack)
	{
		cout << "Player " << active_player << " ALL IN" << endl;
		amount = players[active_player-1].stack;
		players[active_player-1].stack -= amount;
		swap(active_player, non_active_player);
	}else{
		cout << "BET: $" << amount << endl;
		pot += amount;
		players[active_player-1].stack -= amount;
		swap(active_player, non_active_player);
	}
	//cur_bet = amount;
	return amount;
}
*/
int Game::post_flop_bet()
{
	int amount;
	cout << "How much do you want to bet?" << endl;
	cin >> amount;
	if(cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Please enter an integer ONLY." << endl;
		post_flop_bet();
	}	
	if(amount >= players[active_player-1].stack)
	{
		cout << "Player " << active_player << " ALL IN" << endl;
		amount = players[active_player-1].stack;
		players[active_player-1].stack -= amount;

	}else if(amount < big_blind){
		cout << "Bet must be at least equal to the big blind: " << big_blind << endl;
		post_flop_bet();
	}else{
		players[active_player-1].stack -= amount;
		pot += amount;
		cur_bet = amount;
	}
	swap(active_player, non_active_player);
	return amount;
}

void Game::Call()
{
	if(cur_bet >= players[active_player-1].stack)
	{
		cout << "Player " << active_player << " ALL IN" << endl;
		cur_bet = players[active_player-1].stack;
		players[active_player-1].stack -= cur_bet;
		pot += cur_bet;
		players[0].if_call = 0;
		players[1].if_call = 0;
		pre = 0;
		flop = 0;
		turn = 0;
		river = 0;
	
	}else{
	players[active_player-1].stack += players[active_player-1].if_call;
	players[active_player-1].stack -= cur_bet;
	pot -= players[active_player-1].if_call;
	pot += cur_bet;
	players[active_player-1].if_call = 0;
	players[non_active_player-1].if_call = 0;
	}
}


int Game::Raise()
{
	
	int amount;
	raise = 1;
	cout << "How much do you want to raise to?" << endl;
	cin >> amount;
	if(cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Please enter an integer ONLY." << endl;
		Raise();
	}
	players[active_player-1].stack += players[active_player-1].if_call;
	players[non_active_player-1].if_call = cur_bet;
	if(amount >= players[active_player-1].stack)
	{
		cout << "Player " << active_player << " ALL IN" << endl;
		amount = players[active_player-1].stack;
		players[active_player-1].stack -= amount;
		cur_bet = amount;
		pot-=players[active_player-1].if_call;
		pot += cur_bet;
		print_pot();
	}else if(amount < cur_bet * 2){
		cout << "Raise must be greater than or equal to two times the current bet" << endl;
		Raise();
	}else if(amount >= cur_bet * 2){
		cur_bet = amount;
		players[active_player-1].stack -= amount;
		pot-= players[active_player-1].if_call;
		pot += cur_bet;
		print_pot();
	}
	swap(active_player, non_active_player);
	return amount;
}



/*void Game::swap_active()
{
	swap(players[0].active, players[1].active);
}*/


void Game::play(Deck d)
{
	cout << "HAND START"<< endl;
	cout << "------------------" << endl;

	Player p;
	d.shuffle_deck();
	d.values_suits();
	analyze(d);
	flop_card_analyze(d);
<<<<<<< HEAD

	d.shuffle_deck();
	//Flop(d);

	//Turn(d);

	//River(d);

	your_cards_array();

	//their_cards_array();

	//your_hand();

	//their_hand();

	//isThreeOfaKind();

	//showdown(d);
	
=======
>>>>>>> 76e3aa6669a6076cd285e0071c671e14634d7e5f
	pre = 1;
	flop = 1;
	turn = 1;
	river = 1;
	raise = 0;
	set_active_button();	
	
	players[active_player-1].if_call += small_blind;
	players[non_active_player-1].if_call += big_blind;	
	cur_bet = big_blind;
	players[non_active_player-1].stack -= big_blind;
	players[active_player-1].stack -= small_blind;
	pot += big_blind + small_blind;

	cout << "You are PLAYER " << active_player << endl;
	cout << "You are the BUTTON" << endl;
	cout << "You are the Small Blind: " << small_blind << endl;
	system("read -p 'Press Enter to continue.' var");	
	cout << "This is your hand: " << endl;
	game_start(d);
	system("read -p 'Press Enter to continue.' var");
	
<<<<<<< HEAD
	cout << "You are the Small Blind: " << small_blind << endl;

	for(int i = 0; i < 50; i++)
=======
	for(int i = 0; i < 30; i++)
>>>>>>> 76e3aa6669a6076cd285e0071c671e14634d7e5f
	{
		cout << "-----------------------" << endl;
	}
	
	cout << "You are PLAYER " << non_active_player << endl;
	cout << "You are the Big Blind: " << big_blind << endl;
	system("read -p 'Press Enter to continue.' var");	
	cout << "This is your hand: " << endl;
	AI_game(d);
<<<<<<< HEAD

	for(int i = 0; i < 50; i++)
=======
	system("read -p 'Press Enter to continue.' var");
	
	for(int i = 0; i < 30; i++)
>>>>>>> 76e3aa6669a6076cd285e0071c671e14634d7e5f
	{
		cout << "-----------------------" << endl;
	}
	
	bet_decision();
	while(pre)
	{
		decision_prompt();
		if(last_decision == 5)
		{
			pre = 0;
		}
	}
	if(flop)
	{	
		Flop(d);	
		set_active_button();	
		post_flop_decision();
		if(last_decision == 5)
		{
			flop = 0;
		}
	}
	while(flop)
	{
		decision_prompt();
		if(last_decision == 5)
		{
			flop = 0;
		}
	}
	
	if(turn)
	{
		Turn(d);
		set_active_button();	
		post_flop_decision();
		if(last_decision == 5)
		{
			turn = 0;
		}
	}
	while(turn)
	{
		decision_prompt();
		if(last_decision == 5)
		{
			turn = 0;
		}
	}
	if(river)
	{
		River(d);
		set_active_button();	
		post_flop_decision();
		if(last_decision == 5)
		{
			river = 0;
		}
	}
	while(river)
	{
		decision_prompt();
		if(last_decision == 5)
		{
			river = 0;
		}
	}
	if(pre == 0 && flop == 0 && turn == 0 && river == 0 && fold == 0)
	{
		end_of_game(d);
	}
	last_decision = -1;
	raise = 1;
	fold = 0;
	pot = 0;
	cur_bet = 0;
	swap(players[0].button, players[1].button);

}

void Game::Flop(Deck d)
{
	Card c;
	cout << "THE FLOP" << endl;
	for(int i = 0; i < 5; i++)
	{	
	cout << "--------------------------" << endl;
        }	
	d.size = d.size - 4;
	for(int i = 0; i < 3; i++)
	{
		c = d.deal_card();
		flop_cards[i] = c;
		c.print_card(c);
	}
}

void Game::Turn(Deck d)
{
	Card c;
	cout << "THE TURN" << endl;
	for(int i = 0; i < 5; i++)
	{	
	cout << "--------------------------" << endl;
	}
	d.size = d.size - 7;

	c = d.deal_card();
	turn_cards[0] = c;
	c.print_card(c);

	int lower = d.size;
	turn_values[0] = d.values[lower+1];
	turn_suits[0] = d.suits[lower+1];
}

void Game::River(Deck d)
{
	Card c;
	cout << "THE RIVER" << endl;
	for(int i = 0; i < 5; i++)
	{
	cout << "--------------------------" << endl;
	}
	d.size = d.size - 8;
	c = d.deal_card();
	river_cards[0] = c;
	c.print_card(c);

	int lower = d.size;
	river_values[0] = d.values[lower+1];
	river_suits[0] = d.suits[lower+1];

}

void Game::analyze(Deck d)
{

	int _size = 52;
	int lower = _size - 2;

	
	for(int i = lower; i < _size; i++)
	{
		your_values[i-lower] = d.values[i];
		your_suits[i-lower] = d.suits[i];
	}
	int lower_2 = lower - 2;
	for(int j = lower_2; j < lower; j++)
	{
		their_values[j-(lower_2)] = d.values[j];
		their_suits[j-(lower_2)] = d.suits[j];
	}
}

void Game::flop_card_analyze(Deck d)
{
	int _size = 52;

	int lower = _size - 7;

	for(int i = lower; i < lower + 3 ; i++)
	{
		flop_values[i-lower] = d.values[i];
		flop_suits[i-lower] = d.suits[i];

	}
}

void Game::your_cards_array()
{

	for(int i = 0; i < 1; i++)
	{
		total_cards[i+6] = river_values[0];
		for(int i = 0; i < 1; i++)
		{
			total_cards[i+5] = turn_values[0];
			for(int i = 0; i < 2; i++)
			{
				total_cards[i+3] = your_values[i];
				for(int j = 0; j < 3; j++)
				{
					total_cards[j] = flop_values[j];
				} 
			}
		}
	}

	int n = sizeof(total_cards)/sizeof(total_cards[0]);
	sort(total_cards, total_cards+n);

	for(int i = 0; i < 1; i++)
	{
		total_suits[i+6] = river_suits[0];
		for(int i = 0; i < 1; i++)
		{
			total_suits[i+5] = turn_suits[0];
			for(int i = 0; i < 2; i++)
			{
				total_suits[i+3] = your_suits[i];
				for(int j = 0; j < 3; j++)
				{
					total_suits[j] = flop_suits[j];
				} 
			}
		}
	}
	int x = sizeof(total_suits)/sizeof(total_suits[0]);
	sort(total_suits, total_suits+n);
}

void Game::their_cards_array()
{

	for(int i = 0; i < 1; i++)
	{
		their_total_cards[i+6] = river_values[0];
		for(int i = 0; i < 1; i++)
		{
			their_total_cards[i+5] = turn_values[0];
			for(int i = 0; i < 2; i++)
			{
				their_total_cards[i+3] = their_values[i];
				for(int j = 0; j < 3; j++)
				{
					their_total_cards[j] = flop_values[j];
				} 
			}
		}
	}

	int n = sizeof(their_total_cards)/sizeof(their_total_cards[0]);
	sort(their_total_cards, their_total_cards+n);

	for(int i = 0; i < 1; i++)
	{
		their_total_suits[i+6] = river_suits[0];
		for(int i = 0; i < 1; i++)
		{
			their_total_suits[i+5] = turn_suits[0];
			for(int i = 0; i < 2; i++)
			{
				their_total_suits[i+3] = their_suits[i];
				for(int j = 0; j < 3; j++)
				{
					their_total_suits[j] = flop_suits[j];
				} 
			}
		}
	}
	int x = sizeof(their_total_suits)/sizeof(their_total_suits[0]);
	sort(their_total_suits, their_total_suits+n);


}


int Game::your_hand()
{


	int kind = 0;
	int your_val_1 = total_cards[3];
	int your_val_2 = total_cards[4];

	bool straight = false;
	bool flush = false;
	bool four = false;
	bool three = false;
	bool two = false;
	bool one = false;
	int rank = 0;

	int match_vals = 0;
	int match_suits = 0;	

	for(int i = 0; i < 6; i++)
	{
		if(total_cards[i]+1 != total_cards[i+1])
		{
			match_vals = 0;
		}
		if(total_cards[i]+1 == total_cards[i+1])
		{
			match_vals += 1;
			if(match_vals == 4)
			{
				straight = true;
			}
		}
	}

	for(int i = 0; i < 6; i++)
	{	
		if(total_suits[i] != total_suits[i+1])
		{
			match_suits = 0;
		}
		if(total_suits[i] == total_suits[i+1])
		{
			match_suits += 1;
			if(match_suits == 4)
			{
				flush = true;
			}
		}
	}
	
	for(int i = 0; i < 6; i++)
	{
		if(total_cards[i] != total_cards[i+1])
		{
			if(kind == 1)
			{
				if(one == true)
				{
					two = true;
				}
				one = true;
			}else if(kind == 2){
				three = true;
			}else if(kind == 3){
				four = true;
			}
			kind = 0;

		}
		if(total_cards[i] == total_cards[i+1])
		{
			kind++;
		}	
	}
	if(straight && flush)
	{
		rank = 1;
	}
	else if(four == true){
		rank = 2;
	}else if(one == true && three == true){
		rank = 3;
	}else if(flush){
		rank = 4;
	}else if(straight){
		rank = 5;
	}else if(three){
		rank = 6;
	}else if(two){
		rank = 7;
	}else if(one){	
		rank = 8;
	}else{
		rank = 9;
	}
	return rank;
}


int Game::their_hand()
{
	int kind = 0;
	int your_val_1 = their_total_cards[3];
	int your_val_2 = their_total_cards[4];



	bool straight = false;
	bool flush = false;
	bool four = false;
	bool three = false;
	bool two = false;
	bool one = false;
	int rank = 0;

	int match_vals = 0;
	int match_suits = 0;	
	
	for(int i = 0; i < 6; i++)
	{
		if(their_total_cards[i]+1 != their_total_cards[i+1])
		{
			match_vals = 0;
		}
		if(their_total_cards[i]+1 == their_total_cards[i+1])
		{
			match_vals += 1;
			if(match_vals == 4)
			{
				straight = true;
			}
		}
	}

	for(int i = 0; i < 6; i++)
	{	
		if(their_total_suits[i] != their_total_suits[i+1])
		{
			match_suits = 0;
		}
		if(their_total_suits[i] == their_total_suits[i+1])
		{
			match_suits += 1;
			if(match_suits == 4)
			{
				flush = true;
			}
		}
	}


	for(int i = 0; i < 6; i++)
	{
		if(their_total_cards[i] != their_total_cards[i+1])
		{
			if(kind == 1)
			{
				if(one == true)
				{
					two = true;
				}
				one = true;
			}else if(kind == 2){
				three = true;
			}else if(kind == 3){
				four = true;
			}
			kind = 0;

		}
		if(their_total_cards[i] == their_total_cards[i+1])
		{
			kind++;
		}	
	}

	if(straight && flush)
	{
		rank = 1;
	}
	else if(four == true){
		rank = 2;
	}else if(one == true && three == true){
		rank = 3;
	}else if(flush){
		rank = 4;
	}else if(straight){
		rank = 5;
	}else if(three){
		rank = 6;
	}else if(two){
		rank = 7;
	}else if(one){	
		rank = 8;
	}else{
		rank = 9;
	}
	return rank;
}




void Game::end_of_game(Deck d)
{
	cout << "SHOWDOWN" << endl;
	cout << "---------------------" << endl;
	set_active_button();
	cout << "PLAYER " << active_player << " hand: " << endl;
	game_start(d);
	cout << "PLAYER " << non_active_player << " hand: " << endl;
	//their_cards_array();
	//your_cards_array();
	AI_game(d);
	their_cards_array();
	your_cards_array();
	b_rank = your_hand();
	n_rank = their_hand();
	Flop(d);
	Turn(d);
	River(d);
	//their_cards_array();
	//your_cards_array();
	if(b_rank < n_rank)
	{
		cout << "PLAYER " << active_player << " WINS: " << pot << endl;
		players[non_active_player-1].stack += pot;
	}
	if(b_rank > n_rank)
	{
		cout << "PLAYER " << non_active_player << " WINS: " << pot << endl;
		players[non_active_player-1].stack += pot;
	}
	if(b_rank == n_rank)
	{
		int z = rank_analysis();
		if(z == 3)
		{
			cout << "CHOP" << endl;
			players[0].stack += pot*.5;
			players[1].stack += pot*.5;
			return;
		}
		if(z == -1)
		{
			cout << "ERROR" << endl;
		}
		cout << "PLAYER " << z << " WINS: " << pot << endl;
		players[z-1].stack += pot;
	}
}

int Game::rank_analysis()
{
	int nap_val = -1;
	int ap_val = -1;
	if(b_rank == 9 && n_rank == 9)
	{
		for(int i = 0; i < 5; i++)
		{
			if(total_cards[6-i] == their_total_cards[6-i])
			{
				continue;
			}else if(total_cards[6-i] > their_total_cards[6-1])
				{
					return active_player;
				}else if(total_cards[6-i] < their_total_cards[6-1]){
					return non_active_player;
				}
				
			

		}
		return 3;	
	}
	
	if(b_rank == 8 && n_rank == 8)
	{
		for(int i = 0; i < 6; i++)
		{
			if(total_cards[i] != total_cards[i+1])
			{
				continue;
			}else if(total_cards[i] == total_cards[i+1])
				{
					ap_val = total_cards[i]; 	
				}

		}	
		
		for(int i = 0; i < 6; i++)
		{
			if(their_total_cards[i] != their_total_cards[i+1])
			{
				continue;
			}else if(their_total_cards[i] == their_total_cards[i+1])
				{
					nap_val = their_total_cards[i]; 	
				
				}

		}
		if(nap_val > ap_val)
		{
			return non_active_player;
		}
		if(nap_val < ap_val)
		{
			return active_player;
		}
		if(nap_val == ap_val)
		{
			for(int i = 0; i < 5; i++)
			{
				if(total_cards[6-i] == their_total_cards[6-i])
				{
					continue;
				}else if(total_cards[6-i] > their_total_cards[6-i])
				{
					return active_player;
				}else if(total_cards[6-i] < their_total_cards[6-i]){
					return non_active_player;
				}
			}	
<<<<<<< HEAD
		}		
	}
}

=======
			return 3;
		}

	}
	if(b_rank == 7 && n_rank == 7)
	{
		int nap_val_1 = -1;
		int ap_val_1 = -1;
		for(int i = 0; i < 6; i++)
		{
			if(total_cards[i] != total_cards[i+1])
			{
				continue;
			}else if(total_cards[i] == total_cards[i+1])
				{
					if(ap_val > 0)
					{
						ap_val_1 = total_cards[i];

					}else{
						ap_val = total_cards[i]; 	
					}
				}

		}	
		
		for(int i = 0; i < 6; i++)
		{
			if(their_total_cards[i] != their_total_cards[i+1])
			{
				continue;
			}else if(their_total_cards[i] == their_total_cards[i+1])
				{
					if(nap_val > 0)
					{
						nap_val_1 = their_total_cards[i];
					}else{
						nap_val = their_total_cards[i]; 	
					}
				}

		}
		if(nap_val_1 > ap_val_1)
		{
			return non_active_player;
		}
		if(nap_val_1 < ap_val_1)
		{
			return active_player;
		}
		if(nap_val_1 == ap_val_1)
		{
			if(nap_val > ap_val)
			{
				return non_active_player;
			}
			if(nap_val < ap_val)
			{
				return active_player;
			}
			return 3;
		}

	}	
	if(b_rank == 6 && n_rank == 6)
	{
		for(int i = 0; i < 6; i++)
		{
			if(total_cards[i] != total_cards[i+1])
			{
				continue;
			}else if(total_cards[i] == total_cards[i+1])
				{
					ap_val = total_cards[i]; 	
				}

		}	
		
		for(int i = 0; i < 6; i++)
		{
			if(their_total_cards[i] != their_total_cards[i+1])
			{
				continue;
			}else if(their_total_cards[i] == their_total_cards[i+1])
				{
					nap_val = their_total_cards[i]; 	
				
				}

		}
		if(nap_val > ap_val)
		{
			return non_active_player;
		}
		if(nap_val < ap_val)
		{
			return active_player;
		}
		if(nap_val == ap_val)
		{
			for(int i = 0; i < 5; i++)
			{
				if(total_cards[6-i] == their_total_cards[6-i])
				{
					continue;
				}else if(total_cards[6-i] > their_total_cards[6-i])
				{
					return active_player;
				}else if(total_cards[6-i] < their_total_cards[6-i]){
					return non_active_player;
				}
			}	
			return 3;
		}

	}
	if((b_rank == 5 && n_rank == 5)||(b_rank == 1 && n_rank == 1))
	{
		for(int i = 0; i < 6; i++)
		{
			int ap_match_vals = 0;
			int nap_match_vals = 0;
			if(total_cards[i]+1 != total_cards[i+1])
			{
				if(ap_match_vals >= 4)
				{
					ap_val = total_cards[i];		
				}
				ap_match_vals = 0;
			}
			if(total_cards[i]+1 == total_cards[i+1])
			{
				ap_match_vals += 1;
			
			}
			if(their_total_cards[i]+1 != their_total_cards[i+1])
			{
				if(nap_match_vals >= 4)
				{
					nap_val = total_cards[i];		
				}
				nap_match_vals = 0;
			}	
			if(their_total_cards[i]+1 == their_total_cards[i+1])
			{
				nap_match_vals += 1;
			}
		}
		if(ap_val > nap_val)
		{
			return active_player;
		}
		if(ap_val < nap_val)
		{
			return non_active_player;
		}
		if(ap_val == nap_val)
		{
			return 3;
		}
	}
	if(b_rank == 4 && n_rank == 4)
	{
		int ap_match_suits = 0;
		int nap_match_suits = 0;
		for(int i = 0; i < 6; i++)
		{	
			if(total_suits[i] != total_suits[i+1])
			{	
				if(ap_match_suits >= 4)
				{
					ap_val = total_suits[i]; 
				}
				ap_match_suits = 0;
			}
			if(total_suits[i] == total_suits[i+1])
			{
				ap_match_suits += 1;
			}	
			if(their_total_suits[i] != their_total_suits[i+1])
			{
				if(nap_match_suits >= 4)
				{
					ap_val = total_suits[i];
				}
				nap_match_suits = 0;
			}
			if(their_total_suits[i] == their_total_suits[i+1])
			{
				ap_match_suits += 1;
			}
		}
		if(ap_val > nap_val)
		{
			return active_player;
		}
		if(ap_val < nap_val)
		{
			return non_active_player;
		}
		if(ap_val == nap_val)
		{
			return 3;
		}
	}
	if(b_rank == 3 && n_rank == 3)
	{
		int ap_match_val = 0;
		int nap_match_val = 0;
		int ap_val_1 = 0;
		int nap_val_1 = 0;

		for(int i = 0; i < 6; i++)
		{
			if(total_cards[i] != total_cards[i+1])
			{
				if(ap_match_val == 1 && (total_cards[i] > ap_val_1))
				{
						ap_val_1 = total_cards[i];
				}
				if(ap_match_val == 2 && (total_cards[i] > ap_val))
				{
					ap_val = total_cards[i];
				}
				ap_match_val = 0;
			}
			if(total_cards[i] == total_cards[i+1])
			{
				ap_match_val += 1;
			}
			if(their_total_cards[i] != their_total_cards[i+1])
			{
				if(nap_match_val == 1 && (their_total_cards[i] > nap_val_1))
				{
						nap_val_1 = their_total_cards[i];
				}
				if(nap_match_val == 2 && (their_total_cards[i] > nap_val))
				{
					nap_val = their_total_cards[i];
				}
				nap_match_val = 0;
			}
			if(their_total_cards[i] == their_total_cards[i+1])
			{
				nap_match_val += 1;
			}	
		}	
		if(ap_val > nap_val)
		{
			return active_player;
		}
		if(ap_val < nap_val)
		{
			return non_active_player;
		}
		if(ap_val == nap_val)
		{
			if(ap_val_1 > nap_val_1)
			{
				return active_player;
			}
			if(ap_val_1 < nap_val_1)
			{
				return non_active_player;
			}
			if(ap_val_1 == nap_val_1)
			{
				return 3;
			}
		}
	}
		
	return -1;
	
}
>>>>>>> 76e3aa6669a6076cd285e0071c671e14634d7e5f

