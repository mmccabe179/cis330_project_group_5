#ifndef DECK_H_
#define DECK_H_
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <stdlib.h>
#include "Card.h"

class Deck
{
	public:
		Deck();
		int size;
		void original_deck();
		void print_deck();
		void shuffle_deck();
		Card deal_card();
		void values_suits();
		int values[52];
		int suits[52];
		Card deck[52];
};
#endif