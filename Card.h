#ifndef CARD_H_
#define CARD_H_
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <stdlib.h>

using namespace std;

class Card
{
	public:
		Card();
		int create_card();
		void init_card(int);
		void print_card(Card);
		int value;
		int suit;
		int card;
		string values;
		string suits;
};

#endif