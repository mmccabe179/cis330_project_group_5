#include "Deck.h"
#include "Card.h"

Deck::Deck() : size(51) {}


void Deck::original_deck()
{
	/* Intializes the deck Ace through King, to each suit.
	 Total of 13 cards * 4 suits = 52 */
	Deck d;
	Card new_card;
	for(int i = 0; i < 52; i++)
	{
		deck[i].init_card(i);
		//deck[i].print_card(deck[i]);
	}
}

void Deck::print_deck()
{
	/*Prints Card deck[52] within the deck class.*/

	for(int i = 0; i < 52; i++)
	{
		
		deck[i].print_card(deck[i]);	
	}
}


void Deck::shuffle_deck()
{
	/*Randomizes Card deck[52] wtihin the deck class.*/
	srand(time(0));
	original_deck();
	for(int i = 0; i < 52; i++)
	{
		int j = rand() % 52;
		swap(deck[i], deck[j]);
		
		//print_deck();
	}
}
void Deck::values_suits()
{
	/* Intializes the values and suits to each card in Card deck[52] */
	
	int _size = 52;

	for(int i = 0; i < _size; i++)
	{
		suits[i] = deck[i].suit;
		values[i] = deck[i].value;

		//cout << suits[i] << endl;
		//cout << deck[i].value << endl;
	}
}


Card Deck::deal_card()
{
	/* Returns a card from the "top" of the Card deck[52] */
	
	Card c;
	int new_size = size;
	c = deck[new_size];
	size--;
	return c;

}
