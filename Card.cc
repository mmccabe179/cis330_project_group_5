#include "Card.h"
#include "Deck.h"
#include "Player.h"

Card::Card() : value(0), suit(0), card(0)
{}

int Card::create_card()
{
	return card;
}

void Card::init_card(int new_card)
{

	/*Initializes a card using integers 0-52
	as a value between 0-12 and a suit 0-3*/

	if(new_card >= 0 && new_card < 52)
	{
		card = new_card;
		value = card%13;
		//cout << value << endl;
		suit = card/13;
	}
	else
	{
		card = 0;
		value = 0;
		suit = 0;
	}
	
}

void Card::print_card(Card n)
{
	/* Function to print a card value 0-12 as 'K' - 'A' and
	suit 0-3 as 'Hearts' - 'Clubs' */

	string Suits[] = {"Hearts", "Diamonds", "Spades", "Clubs"};
	string Values[] = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
	//cout << n.value << endl;
	cout << Values[n.value] << " of " << Suits[n.suit] << endl;
	
}
