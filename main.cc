#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <stdlib.h>
#include "Card.h"
#include "Deck.h"
#include "Player.h"
#include "Game.h"

using namespace std;



int main()
{
	/* Main function to initialize Card, Deck, Game and Player
	Also sets the button and active player
	Also ensures game play continues until player stack size is 0 */
	
	Card c;
	Card();
	c.create_card();
	Deck d;
	Deck();
	//d.shuffle_deck();
	d.values_suits();
	Player p1;
	Player p2;
	Player();
	Game g;
	Game();
	g.players[0] = p1;
	g.players[1] = p2;
	g.players[0].player = 1;
	g.players[1].player = 2;
	g.players[0].active = 1;
	g.players[0].button = 1;
	Game();
	while(g.players[1].stack > 0 && g.players[0].stack > 0)
	{
		g.play(d);
	}
}
